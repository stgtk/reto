package data

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

// Event represents one instance the application provides it's functionality.
type Event struct {
	// Event Name displayed to the user
	EventName string `json:"event_name" default:"The Play"`
	// URL endpoint for post
	Endpoint string `json:"endpoint" default:"play"`
	// Location of the Content json
	FileLocation string `json:"file_location" default:"play.json"`
	// Location of the mail template
	MailTemplate string `json:"mail_template" default:"mail.tmpl"`
	// Max Persons per performance
	MaxPersons int `json:"max_persons" default:"50"`
	// Admin password
	AdminPassword string `json:"admin_password" default:"::15|part|QUITE|division|82::"`
	// Dates of the performance
	Dates []string `json:"dates" default:"[]"`
}

// AppendWorker appends a reservation to the json file.
// Attention: This function is not thread safe!
// Todo: Make appending a reservation thread safe.
func (e *Event) AppendWorker(rsv Reservation) {
	content := openContent(e.FileLocation)
	content.appendReservation(rsv)
	content.saveContent(e.FileLocation)
}

func (e Event) GetReport() EventReport {
	content := openContent(e.FileLocation)
	return NewEventReport(e, content.Reservations)
}

type EventReport struct {
	Event          string
	MaxPersons     int
	RsvPerDate     map[string][]Reservation
	OccupiedPlaces map[string]OccupiedPlaces
	CreatedAt      time.Time
	Errors         []string
}

func NewEventReport(e Event, reservations []Reservation) EventReport {
	rpt := EventReport{}
	rpt.Event = e.EventName
	rpt.MaxPersons = e.MaxPersons
	rpt.CreatedAt = time.Now()
	rpt.initOccupiedPlaces(e.Dates)
	rpt.fill(e, reservations)
	return rpt
}

func (r *EventReport) initOccupiedPlaces(dates []string) {
	r.OccupiedPlaces = make(map[string]OccupiedPlaces)
	for _, date := range dates {
		r.OccupiedPlaces[date] = NewOccupiedPlaces(r.MaxPersons)
	}
}

func (r *EventReport) fill(e Event, reservations []Reservation) {
	r.RsvPerDate = make(map[string][]Reservation)
	for _, date := range e.Dates {
		r.RsvPerDate[date] = []Reservation{}
	}

	for _, rsv := range reservations {
		if r.RsvPerDate[rsv.Date] == nil {
			errMsg := fmt.Sprintf("date %s is not specified in the event config", rsv.Date)
			logrus.Error(errMsg)
			continue
		}
		r.RsvPerDate[rsv.Date] = append(r.RsvPerDate[rsv.Date], rsv)
		r.OccupiedPlaces[rsv.Date] = r.OccupiedPlaces[rsv.Date].AddUsage(rsv.Places)
	}
}

func (r *EventReport) errorOccurred(msg string) {
	logrus.Error(fmt.Sprintf("error while processing event report: %s", msg))
	r.Errors = append(r.Errors, msg)
}

type OccupiedPlaces struct {
	Current int
	Left    int
	Max     int
	Status  string
}

func NewOccupiedPlaces(max int) OccupiedPlaces {
	return OccupiedPlaces{
		Current: 0,
		Left:    max,
		Max:     max,
		Status:  "ok",
	}
}

func (o OccupiedPlaces) AddUsage(places int) OccupiedPlaces {
	o.Current += places
	o.calculate()
	return o
}

func (o *OccupiedPlaces) calculate() {
	o.Left = o.Max - o.Current
	if o.Current == o.Max {
		o.Status = "full"
	}
}
