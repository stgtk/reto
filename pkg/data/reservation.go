package data

import (
	"errors"
	"github.com/sirupsen/logrus"
	"regexp"
	"strconv"
	"time"
)

// Reservation represents a single reservation to a event.
type Reservation struct {
	// The name of the event (same as in the Config).
	Event string `json:"event"`
	// Name of the customer.
	Name string `json:"name"`
	// Number of places to be reserved.
	Places int `json:"places"`
	// The date of the performance.
	Date string `json:"date"`
	// Mail of the customer.
	Mail string `json:"mail"`
	// Additional notes by the customer.
	Notes string `json:"notes"`
	// Date and time of the submission
	ReservedAt time.Time `json:"reserved_at"`
}

// Check if the fields were filled out correctly. If so, the data is parsed and a Reservation struct is returned.
func NewReservation(event, name, places, date, mail, notes string) (rsv Reservation, err error) {
	rsv.Event = event

	if len(name) < 1 {
		return rsv, errors.New("name empty")
	}
	rsv.Name = name

	plc, err := strconv.Atoi(places)
	if err != nil {
		return rsv, errors.New("places is not a number")
	}
	rsv.Places = plc

	if len(date) < 1 {
		return rsv, errors.New("event date empty")
	}
	rsv.Date = date

	expr := regexp.MustCompile(`(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)`)
	if !expr.MatchString(mail) {
		return rsv, errors.New("mail address invalid")
	}
	rsv.Mail = mail
	rsv.Notes = notes
	rsv.ReservedAt = time.Now()

	return rsv, nil
}

// Fields returns the logrus.Fields for logging.
func (r Reservation) Fields() logrus.Fields {
	return logrus.Fields{
		"event":  r.Event,
		"name":   r.Name,
		"places": r.Date,
		"date":   r.Mail,
		"notes":  r.Notes,
	}
}

func (r Reservation) FmtReservedAt() string {
	return r.ReservedAt.Format("1 Jan 2006 15:04")
}
