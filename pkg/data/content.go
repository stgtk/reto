package data

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
)

// Content represents the struct, the reservations for a specific event are stored.
// This data is saved in a json file which path is defined by the Event.FileLocation.
type Content struct {
	Event        string        `json:"event"`
	Reservations []Reservation `json:"reservations"`
}

func openContent(path string) Content {
	content := Content{}
	configJson, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.Error("Error while reading from disk: ", err)
	}

	err = json.Unmarshal(configJson, &content)
	if err != nil {
		logrus.Fatal("Error while parsing JSON: ", err)
	}

	logrus.Debug(fmt.Sprintf("Content file %s for event %s loaded", path, content.Event))
	return content
}

func (c *Content) appendReservation(reservation Reservation) {
	logrus.WithFields(reservation.Fields()).Debug("new reservation to be added to content")
	c.Reservations = append(c.Reservations, reservation)
	logrus.Debug("reservation added to content")
}

func (c Content) saveContent(path string) {
	configJson, err := json.Marshal(c)
	if err != nil {
		logrus.Error("Error while JSON marshal: ", err)
	}
	err = ioutil.WriteFile(path, configJson, 0644)
	if err != nil {
		logrus.Error("Error while write JSON to disk: ", err)
	}
	logrus.Debug(fmt.Sprintf("content saved to %s", path))
}
