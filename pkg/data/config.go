package data

import (
	"encoding/json"
	"fmt"
	"github.com/creasty/defaults"
	"github.com/sirupsen/logrus"
	"io/ioutil"
)

// Config provides the configuration for the program.
type Config struct {
	// Running port for the HTTP server
	Port int `json:"port" default:"9080"`
	// Mail Address
	MailAddress string `json:"mail_address" default:"info@example.com"`
	// Mail password
	MailPassword string `json:"mail_password" default:"secret"`
	// SMTP Server
	SmtpHost string `json:"smtp_host" default:"smtp.example.com"`
	// SMTP Port
	SmtpPort int `json:"smtp_port" default:"587"`
	// SMTP User
	SmtpUser string `json:"smtp_user" default:"info@example.com"`
	// Separate Events the application provides the data function
	Events []Event `json:"events" default:"[]"`
}

// Returns a new Config with default values.
func NewConfig() *Config {
	config := Config{}
	if err := defaults.Set(&config); err != nil {
		logrus.Error("Default generation failed: ", err)
	}
	event := Event{}
	if err := defaults.Set(&event); err != nil {
		logrus.Error("Default generation failed: ", err)
	}
	config.Events = append(config.Events, event)
	return &config
}

// OpenConfig opens the Config from a given path.
func OpenConfig(path string) Config {
	config := Config{}
	configJson, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.Error("Error while reading from disk: ", err)
	}

	err = json.Unmarshal(configJson, &config)
	if err != nil {
		logrus.Fatal("Error while parsing JSON: ", err)
	}

	logrus.Debug(fmt.Sprintf("config from %s loaded", path))
	return config
}

// SaveConfig saves the Config to the given path.
func (c Config) SaveConfig(path string) {
	configJson, err := json.Marshal(c)
	if err != nil {
		logrus.Error("Error while JSON marshal: ", err)
	}
	err = ioutil.WriteFile(path, configJson, 0644)
	if err != nil {
		logrus.Error("Error while write JSON to disk: ", err)
	}
}
