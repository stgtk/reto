package server

import (
	"errors"
	"fmt"
	rice "github.com/GeertJohan/go.rice"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/stgtk/reto/pkg/data"
	"html/template"
	"net/http"
	"strconv"
)

// Server is a HTTP server providing the data functionality.
type Server struct {
	Config           data.Config
	OverviewTemplate *template.Template
	ErrorTemplate    *template.Template
	LoginTemplate    *template.Template
	ReportTemplate   *template.Template
}

// NewServer returns a Server with the default values.
func NewServer(config data.Config) *Server {
	return &Server{
		Config:           config,
		OverviewTemplate: getOverviewTemplate(),
		ErrorTemplate:    getErrorTemplate(),
		LoginTemplate:    getLoginTemplate(),
		ReportTemplate:   getReportTemplate(),
	}
}

// Server runs the HTTP server.
func (s *Server) Serve() {
	router := mux.NewRouter()
	router.HandleFunc("/", s.getDefaultEndpoint)
	router.HandleFunc("/reservation/{id}/endpoint", s.getFormEndpoint)
	router.HandleFunc("/reservation/{id}/adm", s.getAdminEndpoint)
	router.HandleFunc("/reservation/{id}/login", s.getLoginEndpoint)
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(rice.MustFindBox("static").HTTPBox())))

	addr := fmt.Sprintf(":%d", s.Config.Port)
	srv := &http.Server{
		Addr:    addr,
		Handler: router,
	}
	logrus.Debug("about to start http server")
	if err := srv.ListenAndServe(); err != nil {
		logrus.Fatal(err)
	}
}

func (s Server) getDefaultEndpoint(w http.ResponseWriter, r *http.Request) {
	if _, err := fmt.Fprint(w, "Nothing"); err != nil {
		logrus.Error(err)
	}
}

// Todo: This function is a mess and has to be refactored.
func (s Server) getFormEndpoint(w http.ResponseWriter, r *http.Request) {
	logrus.Debug("new form post request")
	if r.Method == "GET" {
		logrus.Warn("this was a GET request")
		return
	}

	event, err := s.parseIdToEvent(r)
	if err != nil {
		logrus.Error(err)
		return
	}

	if err := r.ParseForm(); err != nil {
		logrus.Error(err)
		return
	}

	if err := checkCaptcha(w, r); err != nil {
		logrus.Debug(err)
		return
	}

	rsv, err := data.NewReservation(
		event.Endpoint,
		r.Form["name"][0],
		r.Form["places"][0],
		r.Form["date"][0],
		r.Form["mail"][0],
		r.Form["notes"][0])
	if err != nil {
		logrus.Warn("parsing of reservation was unsuccessful: ", err)
		s.renderError(w, event.EventName, err.Error())
		return
	}

	go s.doReservation(event, rsv)

	if err := s.OverviewTemplate.Execute(w, rsv); err != nil {
		logrus.Error(err)
	}
}

// Checks the captcha, if wrong a simple error message is presented to the customer.
func checkCaptcha(w http.ResponseWriter, r *http.Request) error {
	if n, err := strconv.Atoi(r.Form["check"][0]); n == 7 && err == nil {
		return nil
	}
	if _, err := fmt.Fprint(w, "Ungültige Eingabe, haben Sie die Rechnung richtig gelöst?"); err != nil {
		logrus.Error(err)
	}
	return errors.New(fmt.Sprintf("captcha was not solved correctly input: %s", r.Form["check"][0]))
}

// renderError shows a error page to the customer with the given message.
func (s *Server) renderError(w http.ResponseWriter, eventName string, message string) {
	usrErr := map[string]string{
		"Event":   eventName,
		"Message": message,
	}
	if err := s.ErrorTemplate.Execute(w, usrErr); err != nil {
		logrus.Error(err)
	}
}

// doReservation saves the reservation and sends an Mail.
// It only should be called when all input is checked for correctness.
func (s *Server) doReservation(event data.Event, rsv data.Reservation) {
	event.AppendWorker(rsv)
	mail := ReservationMail{
		Mail: Mail{
			MailAddress:  s.Config.MailAddress,
			SmtpHost:     s.Config.SmtpHost,
			SmtpPort:     s.Config.SmtpPort,
			SmtpUser:     s.Config.SmtpUser,
			SmtpPassword: s.Config.MailPassword,
			EventName:    event.EventName,
		},
		MailTemplate: event.MailTemplate,
		Reservation:  rsv,
	}
	mail.Send(rsv)
}

func (s Server) getAdminEndpoint(w http.ResponseWriter, r *http.Request) {
	event, err := s.parseIdToEvent(r)
	if err != nil {
		logrus.Error(err)
	}
	dt := map[string]string{
		"Event": event.EventName,
	}
	if err := s.LoginTemplate.Execute(w, dt); err != nil {
		logrus.Error("error while execute admin login", err)
	}
}

func (s Server) getLoginEndpoint(w http.ResponseWriter, r *http.Request) {
	logrus.Debug("new login post request")
	if r.Method == "GET" {
		logrus.Warn("this was a GET request")
		return
	}

	event, err := s.parseIdToEvent(r)
	if err != nil {
		logrus.Error(err)
		return
	}

	if err := r.ParseForm(); err != nil {
		logrus.Error(err)
		return
	}
	pwd := r.Form["pwd"]
	if len(pwd) != 1 {
		logrus.Debug("No pwd in login form request")
		return
	}

	if pwd[0] != event.AdminPassword {
		if _, err := fmt.Fprint(w, "Login fehlgeschlagen."); err != nil {
			logrus.Error(err)
		}
		return
	}

	if err := s.ReportTemplate.Execute(w, event.GetReport()); err != nil {
		logrus.Error(err)
	}
}

func (s Server) parseIdToEvent(r *http.Request) (data.Event, error) {
	params := mux.Vars(r)
	for _, evt := range s.Config.Events {
		if evt.Endpoint == params["id"] {
			return evt, nil
		}
	}
	return data.Event{}, errors.New("no event for given url found")
}

func getOverviewTemplate() *template.Template {
	templateBox, err := rice.FindBox("static")
	if err != nil {
		logrus.Fatal(err)
	}
	templateString, err := templateBox.String("overview.html")
	if err != nil {
		logrus.Fatal(err)
	}
	tpl, err := template.New("overview").Parse(templateString)
	if err != nil {
		logrus.Fatal(err)
	}
	return tpl
}

func getErrorTemplate() *template.Template {
	templateBox, err := rice.FindBox("static")
	if err != nil {
		logrus.Fatal(err)
	}
	templateString, err := templateBox.String("error.html")
	if err != nil {
		logrus.Fatal(err)
	}
	tpl, err := template.New("error").Parse(templateString)
	if err != nil {
		logrus.Fatal(err)
	}
	return tpl
}

func getLoginTemplate() *template.Template {
	templateBox, err := rice.FindBox("static")
	if err != nil {
		logrus.Fatal(err)
	}
	templateString, err := templateBox.String("login.html")
	if err != nil {
		logrus.Fatal(err)
	}
	tpl, err := template.New("login").Parse(templateString)
	if err != nil {
		logrus.Fatal(err)
	}
	return tpl
}

func getReportTemplate() *template.Template {
	templateBox, err := rice.FindBox("static")
	if err != nil {
		logrus.Fatal(err)
	}
	templateString, err := templateBox.String("report.html")
	if err != nil {
		logrus.Fatal(err)
	}
	tpl, err := template.New("report").Parse(templateString)
	if err != nil {
		logrus.Fatal(err)
	}
	return tpl
}
