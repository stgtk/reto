package server

import (
	"bytes"
	"fmt"
	"github.com/sirupsen/logrus"
	mail "github.com/xhit/go-simple-mail"
	"gitlab.com/stgtk/reto/pkg/data"
	"html/template"
	"io/ioutil"
	"time"
)

type Mail struct {
	MailAddress  string
	SmtpHost     string
	SmtpPort     int
	SmtpUser     string
	SmtpPassword string
	EventName    string
}

// Sends a mail
func (m Mail) Send(recipientAdr []string, content string, subject string) {
	server := mail.NewSMTPClient()
	server.Host = m.SmtpHost
	server.Port = m.SmtpPort
	server.Username = m.SmtpUser
	server.Password = m.SmtpPassword
	server.Encryption = mail.EncryptionNone

	server.KeepAlive = false
	server.ConnectTimeout = 3 * time.Second
	server.SendTimeout = 10 * time.Second
	client, err := server.Connect()
	if err != nil {
		logrus.Error(err)
	}

	msg := mail.NewMSG()
	for _, rcp := range recipientAdr {
		msg.AddTo(rcp)
	}
	msg.SetFrom(m.MailAddress).SetSubject(subject)
	msg.SetBody(mail.TextPlain, content)
	if err := msg.Send(client); err != nil {
		logrus.Error(err)
	} else {
		logrus.Debug(fmt.Sprintf("mail %s successfully sent to %s", subject, recipientAdr))
	}
}

type ReservationMail struct {
	Mail
	MailTemplate string
	Reservation  data.Reservation
	EventName    string
}

// SendReservation generates and sends message to notify customer of his/her successful reservation.
func (m ReservationMail) Send(reservation data.Reservation) {
	msg, err := m.getMailBody()
	if err != nil {
		logrus.Error(err)
	}

	m.Mail.Send([]string{m.Reservation.Mail}, msg, "Ihre Reservation")
}

// getMailBody loads template specified by the path and returns the message
func (m ReservationMail) getMailBody() (string, error) {
	raw, err := ioutil.ReadFile(m.MailTemplate)
	if err != nil {
		logrus.Error(err)
	}
	tmpl, err := template.New("mail").Parse(string(raw))
	if err != nil {
		logrus.Error(err)
	}

	buffer := new(bytes.Buffer)
	err = tmpl.Execute(buffer, m)
	return buffer.String(), err
}
