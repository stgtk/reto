package main

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/stgtk/reto/pkg/data"
	"gitlab.com/stgtk/reto/pkg/server"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "reto"
	app.Usage = "Reservation Tool"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config, c",
			Usage: "path to config file",
		},
		cli.BoolFlag{
			Name:  "debug, d",
			Usage: "enable debug mode",
		},
	}
	app.Action = func(c *cli.Context) error {
		return runServer(c.String("config"), c.Bool("debug"))
	}
	app.Commands = []cli.Command{
		{
			Name:  "new",
			Usage: "create default config",
			Action: func(c *cli.Context) error {
				return newConfig(c.Args().First())
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func runServer(path string, debugEnabled bool) error {
	if debugEnabled {
		logrus.SetLevel(logrus.DebugLevel)
	}
	cfg := data.OpenConfig(path)
	srv := server.NewServer(cfg)
	srv.Serve()
	return nil
}

func newConfig(path string) error {
	cfg := data.NewConfig()
	cfg.SaveConfig(path)
	return nil
}
